import React from 'react';
import ProductItem from '../components/ProductItem';
import ProductsList from '../components/ProductsList';

export default class ProductsContainer extends React.Component {
  render() {
    const { products, addToCart } = this.props;
    return (
      <ProductsList title="Products">
        {products.map(product => (
          <ProductItem
            key={product.id}
            product={product}
            onAddToCartClicked={addToCart}
          />
        ))}
      </ProductsList>
    );
  }
}
