import React from 'react';
import Cart from '../components/Cart';

export default class CartContainer extends React.Component {
  render() {
    const { checkout, products, total } = this.props;
    return (
      <Cart products={products} total={total} onCheckoutClicked={checkout} />
    );
  }
}
