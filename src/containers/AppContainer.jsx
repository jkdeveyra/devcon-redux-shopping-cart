import React from 'react';
import { cloneDeep } from 'lodash';
import ProductItem from '../components/ProductItem';
import ProductsList from '../components/ProductsList';
import shop from '../api/shop';
import CartContainer from './CartContainer';
import ProductsContainer from './ProductsContainer';

export default class AppContainer extends React.Component {
  state = {
    products: new Map(),
    cart: new Map(),
    total: 0.0
  };

  componentDidMount() {
    shop.getProducts(products => {
      const map = new Map();
      products.forEach(product => {
        map.set(product.id, product);
      });
      this.setState({ products: map });
    });
  }

  handleAddToCart = id => {
    const selected = cloneDeep(this.state.products.get(id));
    if (selected && selected.inventory > 0) {
      const products = cloneDeep(this.state.products);
      selected.inventory -= 1;
      products.set(selected.id, selected);

      const cart = cloneDeep(this.state.cart);

      const item = cart.get(selected.id) || { ...selected, quantity: 0 };
      item.quantity += 1;

      cart.set(selected.id, item);
      this.setState({ products: products, cart: cart });
    }
  };

  handleCheckout = products => {
    shop.buyProducts(products, () => {
      this.setState({ cart: new Map() });
    });
  };

  getProducts = () => {
    return Array.from(this.state.products.values());
  };

  getCartItems = () => {
    return Array.from(this.state.cart.values());
  };

  getTotal = () => {
    let total = 0.0;
    this.getCartItems().forEach(item => {
      total += item.price * item.quantity;
    });
    return total;
  };

  render() {
    return (
      <div>
        <h2>Shopping Cart Example</h2>
        <hr />
        <ProductsContainer
          products={this.getProducts()}
          addToCart={this.handleAddToCart}
        />
        <hr />
        <CartContainer
          products={this.getCartItems()}
          checkout={this.handleCheckout}
          total={this.getTotal()}
        />
      </div>
    );
  }
}
